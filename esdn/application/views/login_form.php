<div align="center">	
	<form action="loginAction" method="POST">
		<!-- token acessing -->
		<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
		
		<div><h3> Login Form </h3></div>
		<div>
			<label for="uname"><b> Username </b></label>
			<input type="text" placeholder="enter username here" name="uname" autocomplete="off" autofocus required/>
		</div>
		<div class="sameline">
	        <label for="pwd"><b> Password </b></label>
	        <input type="password" name="pwd" required/>
		</div>
		<div>
			<input type="submit" value="Login"/>
			<br>
		</div>
	</form>
</div>