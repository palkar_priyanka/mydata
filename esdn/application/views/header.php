<!DOCTYPE html>
<html>
<head>
	<title>ESDA</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/common_style.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/font-awesome.min.css'); ?>">
  <script src="<?php echo base_url('assets/lib/js/jquery.min.js'); ?>">  </script>
  <script src="<?php echo base_url('assets/lib/js/popper.min.js'); ?>"> </script>
  <script src="<?php echo base_url('assets/lib/js/bootstrap.min.js'); ?>"> </script>
</head>
<body>
<div class="container-fluid p-2 bg-dark text-white">
	<div class="row">
		<div class="col-md-6">
		</div>
		<div class="col-md-6 text-right">
			<img class="img-esdn" src="<?php echo base_url('assets/images/phone-call.png'); ?>" alt="phone-call"><small>  + (91) 9967957002</small>
			<small><img class="img-esdn" src="<?php echo base_url('assets/images/email.png'); ?>" alt="email">    info@esda@.com</small>
			<small><img class="img-esdn" src="<?php echo base_url('assets/images/pin.png'); ?>" alt="pin">       Mumbai</small>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-2 mx-5">
		<a href="<?php echo base_url(); ?>">
    		<img src="<?php echo base_url('assets/images/ESDACART.png'); ?>" alt="ESDACART">
  		</a>
	</div>
	<div class="col-md-4 my-3">
		<form id="search-form" method="post" action="">
			<div class="input-group">
				<input type="text" class="form-control search-form" placeholder="Search">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-primary search-btn" data-target="#search-form" name="q"><i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</form>
	</div>
	<div class="col p-1 my-3">
		<img src="<?php echo base_url('assets/images/heart.png'); ?>" alt="heart" style="height: 25px;padding-left: 100px;">
	</div>
	<div class="col my-3">
		<img src="<?php echo base_url('assets/images/shopping-cart.png'); ?>" alt="shopping-cart" style="height: 25px;margin-left: 55px;">
	</div>
	<div class="col my-3">
		<b>My Cart</b><p><small>0</small></p>
	</div>
	<?php if($this->session->userdata("username"))  { ?>

	<div class="col-md-2 my-3">
		<a href="<?php echo base_url('admin/logout'); ?>">
		<button class="btn btn-primary">logout</button></a>
	</div>
	<?php }else{?>
	<div class="col-md-2 my-3">
		<a href="<?php echo base_url('admin/login'); ?>">
		<button class="btn btn-primary"><i class="fa fa-user fa-6" aria-hidden="true"></i>Register or Sign in</button></a>
	</div>
<?php } ?>
</div>
<hr class="line">

<div class="notification">

  <?php if($this->session->flashdata('success_msg')) { ?>
    <p class="bg-success text-white text-center">
      <?php echo $this->session->flashdata('success_msg'); ?>
    </p>
  <?php } ?>


  <?php if($this->session->flashdata('error_msg')) { ?>
    <p class="bg-danger text-white text-center">
      <?php echo $this->session->flashdata('error_msg'); ?>
    </p>
  <?php } ?>

</div>