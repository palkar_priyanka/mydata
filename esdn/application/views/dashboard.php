<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 text-center">
			<h1 style="font-weight: 800;color:#FF8E0A">ADD PRODUCTS</h1>
		</div>
	</div>
</div>
<br>
<hr class="line">
<div class="container mt-4">
	<form method="POST" action="<?php echo base_url('admin/addProduct') ?>" enctype="multipart/form-data">
	<div class="row ">
		<div class="col-lg-6 col-md-6 col-sm-5 text-center bg-light mt-3">
			<img src="<?php echo base_url('assets/images/upload.png'); ?>" class="img-fluid rounded pt-5" alt="upload">
            <input type="file" name="product_img" value="<?php echo set_value('image');?>" class="form-control"/>
			<p>(Jpg,Png,mp4,gif)</p>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6  mt-3 ">
			<h3>Product Name </h3>
			<input type="text" name="product_name" id="product_name" class="form-control" placeholder="Enter your product detail here">
            <?php echo form_error('product_name')?>
			
			<h3>Product Description </h3>
			<input type="text" name="product_description" id="product_description" class="form-control" placeholder="Enter your product description here">
            <?php echo form_error('product_description')?>
			
			<div class="row ">
				<div class="col-lg-6 col-md-6 col-sm-5">
					<h3>Product Price </h3>
					<input type="text" name="product_price" id ="product_price" class="form-control" placeholder="Enter product price here">
		            <?php echo form_error('product_price')?>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h3>Product Avalibility</h3>
					<input type="text" name="product_avalibility" id="product_avalibility" class="form-control" placeholder="Enter product avalibility">
				    <?php echo form_error('product_price')?>
				</div>
			</div>
			<div class="row mt-2">
				<div class="col-lg-6 col-md-6 col-sm-6 text-right">
					<button class="btn btn-primary" type="submit">Add</button>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>
<br>
<br>
<hr class="line">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 text-center">
			<h1 style="font-weight: 800;color:#FF8E0A">OUR PRODUCTS</h1>
		</div>
	</div>
</div>
<br>
<hr class="line">
<div class="container mt-4">
	<div class="row">
		<?php 
		if(!empty($product)){
			foreach($product as $value) { ?>
		<div class="col-md-3">
			<div class="row">
				<div class="col-md-9 p-3">
					<img src="<?php echo $value['image']; ?>" alt="phone" class="img-fluid">
				</div>
				<div class="col-md-3 mt-5 p-1">
					<img src="<?php echo base_url('assets/images/Group 194.png'); ?>" alt="phone" style="height: 40px;padding:5px">
					<img src="<?php echo base_url('assets/images/Group 193.png'); ?>" alt="phone" style="height: 37px;padding:5px">
					<img src="<?php echo base_url('assets/images/Group 192.png'); ?>" alt="phone" style="height: 30px;padding:5px">
				</div>
				<div class="col-md-12">
					<h5><b><?php echo $value['product_name']; ?></b></h5>
					<h3><?php echo $value['product_price']; ?></h3>
					<h5>Avalibility <?php echo $value['product_avalibility']; ?></h5>
				</div>
			</div>
		</div>
		<?php 
			}
		} 
		?>
	</div>
</div>