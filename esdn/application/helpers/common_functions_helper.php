<?php

//if this function return true then user is logged in
function admin_authorization() {
    if (isset($_SESSION["username"]) && $_SESSION["username"] != null) {
        return true;
    }  
    return false;
}

function authorize() {
    if(admin_authorization() == false) {
        redirect('admin/login');
    }
}

function pagenationConfig($base_url, $count = 0) {
    $config = array();
    $config["base_url"] = site_url($base_url);
    $config["total_rows"] = $count;
    $config["per_page"] = 10;
    $config["uri_segment"] = 2;
    $config['page_query_string'] = TRUE;
    $config['query_string_segment'] = 'offset';
    // Bootstrap 4 Pagination CSS
    $config['full_tag_open'] = '<ul class="pagination float-right mr-5">';
    $config['full_tag_close'] = '</ul>';
    $config['attributes'] = ['class' => 'page-link'];
    $config['first_link'] = false;
    $config['last_link'] = false;
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = '</li>';
    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
    $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';

    return $config;
}

?>