<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if(admin_authorization()) {
            redirect(site_url('admin/dashboard'));
        }
    }
       
    public function index() {
    	$this->load->view('header');
    	$this->load->view('landing_page');
        $this->load->view('footer');
    }
}

?>