<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function login(){

        if(admin_authorization()) {
            redirect(site_url('admin/dashboard'));
        }

        $this->csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $data = array (
            'title' => "Admin Login", 
            'page' => "login_form",
            'csrf' => $this->csrf
        );
        $this->load->view('header');
        $this->load->view('login_form', $data);
        $this->load->view('footer');
    }

    public function loginAction(){
        $this->load->model('AdminModel');
        $form_data = $this->input->post(); 
        if(isset($form_data["uname"]) && $form_data["uname"] != "" &&
            isset($form_data["pwd"]) && $form_data["pwd"] != "") { //validate data if correct pass data to model
            $admin_data = $this->AdminModel->getAdminData($form_data); //pass data to model for sql

            if(count($admin_data) > 0) { // if records found then redirect to dashboard
                $this->session->set_userdata("username", $admin_data[0]->username);
                $this->session->set_flashdata('success_msg', 'Logged in successfully');
                redirect("admin/dashboard");
            }else{ //else redirect to login with error message
                $this->session->set_flashdata('error_msg', 'Username or password is incorrect');
                redirect("admin/login");
            }
        }
    }

    public function dashboard(){
        admin_authorization();
        $this->load->model('AdminModel');
        $product['product'] = $this->AdminModel->getProduct();
        $this->load->view('header');
        $this->load->view('dashboard',$product);
        $this->load->view('footer');
    }

    public function logout(){
        admin_authorization();
        $this->session->sess_destroy();
        $this->session->set_flashdata('success_msg', 'Logout successfully');
        redirect("home");
    }

    public function addProduct(){
        // $this->upload->initialize($config);
        $config=[       
            'upload_path' => './uploads',
            'allowed_types' => 'gif|jpg|png|jpeg',
        ];
        $this->load->library('upload', $config);
        $this->form_validation->set_error_delimiters();
        $this->upload->do_upload();
        $data = $this->input->post();
        $info = $this->upload->data();
        $image_path = base_url("uploads".$info['raw_name'].$info['file_ext']);
        $data['image'] = $image_path;
        unset($data['submit']);

        $this->form_validation->set_rules('product_name','Product Name','required');
        $this->form_validation->set_rules('product_description','Product Description','required');
        $this->form_validation->set_rules('product_price','Product Price','required');
        $this->form_validation->set_rules('product_avalibility','product_avalibility','required');
        if($this->form_validation->run() == false){
            $this->session->set_flashdata('error_msg','Something went wrong');
            redirect(base_url().'admin/dashboard');
        }else{
            $formArray['product_name'] = $this->input->post('product_name');
            $formArray['product_description'] = $this->input->post('product_description');
            $formArray['product_price'] = $this->input->post('product_price');
            $formArray['product_avalibility'] = $this->input->post('product_avalibility');
            $formArray['image']=$image_path;
            $formArray['added_date'] = date('Y-m-d h:i:s');
            $formArray['updated_date'] = date('Y-m-d h:i:s');
            $this->load->model('AdminModel');
            $result = $this->AdminModel->add($formArray);
            if($result){
                $this->session->set_flashdata('success_msg','Something went wrong');
                redirect(base_url().'admin/show');
            }else{
                $this->session->set_flashdata('error_msg','Something went wrong');
                redirect(base_url().'admin/dashboard'); 
            }
        }
    }
    public function show(){
        $this->load->model('AdminModel');
        $product['product'] = $this->AdminModel->getProduct();
        $this->load->view('header');
        $this->load->view('dashboard',$product);
        $this->load->view('footer');
    }
}

?>