@extends('layouts.app')

@section('content')
<div class="container">
	<h3>Add Student</h3>
	<hr>
</div>
	
<div class="container" style="padding-top: 10px">
<form method="post" name="addStudent" action="http://127.0.0.1:8000/student/addAction'?>">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="firstname"><b>First Name</b></label>
				<input type="text" name="firstname" placeholder="Enter first name" class="@error('firstname') is-invalid @enderror" class="form-control">
				@error('firstname')
				    <div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
			<div class="form-group">
				<label for="lastname"><b>Last Name</b></label>
				<input type="text" name="lastname" placeholder="Enter last name" class="@error('lastname') is-invalid @enderror" class="form-control">
				@error('lastname')
				    <div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
			<div class="form-group">
				<label for="email"><b>Email</b></label>
				<input class="form-control" type="text" name="email" placeholder="Enter email address" class="@error('email') is-invalid @enderror" autocomplete="off">
				@error('email')
				    <div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
			<div class="form-group">
				<label for="city"><b>City</b></label>
				<input class="form-control" type="text" name="city" placeholder="Enter city" class="@error('city') is-invalid @enderror">
				@error('city')
				    <div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
			<div class="form-group">
				<button class="btn btn-primary">Add</button>
				<a href="http://127.0.0.1:8000/student/listing'?>" class="btn-secondary btn">Cancel</a>
			</div>
		</div>
	</div>
</form>
</div>
@endsection