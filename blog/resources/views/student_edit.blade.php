@extends('layouts.app')

@section('content')
<div class="container">
	<h3>Add Student</h3>
	<hr>
</div>
<?php
foreach ($result as $value) {
	$editData = $value;
}
?>
<div class="container" style="padding: 0 100px 0 100px;">
<form method="POST" name="editStudent" action="http://127.0.0.1:8000/student/editAction">
	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
	<input type="hidden" name="id" value="<?php echo $editData->id; ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group" >
				<label for="first_name"><b>First Name</b></label>
				<input class="form-control" type="text" name="first_name" value="<?php echo $editData->first_name; ?>" placeholder="Enter first name" class="@error('first_name') is-invalid @enderror">
				@error('first_name')
				    <div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
			<div class="form-group">
				<label for="last_name"><b>Last Name</b></label>
				<input class="form-control" type="text" name="last_name" value="<?php echo $editData->last_name; ?>" placeholder="Enter last name" class="@error('last_name') is-invalid @enderror">
				@error('last_name')
				    <div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
			<div class="form-group">
				<label for="email"><b>Email</b></label>
				<input class="form-control" type="text" name="email" value="<?php echo $editData->email; ?>" placeholder="Enter email address" class="@error('email') is-invalid @enderror" autocomplete="off">
				@error('email')
				    <div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
			<div class="form-group" >
				<label for="city"><b>City</b></label>
				<input class="form-control" type="text" name="city" value="<?php echo $editData->city; ?>" placeholder="Enter city" class="@error('city') is-invalid @enderror">
				@error('city')
				    <div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
			<div class="form-group">
				<button class="btn btn-primary">Update</button>
				<a href="http://127.0.0.1:8000/student/listing" class="btn-secondary btn">Cancel</a>
			</div>
		</div>
	</div>
</form>
</div>
@endsection