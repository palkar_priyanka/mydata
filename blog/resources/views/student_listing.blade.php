@extends('layouts.app')

@section('content')
<div class="container" style="padding-top: 10px">
        <div class="row">
        <div class="col-md-12">
            <h3>Student list</h3>
            <div class="row">
                <div class="col-12 text-right">
                    <a href="http://127.0.0.1:8000/student/add" class="btn btn-primary">Add</a>
                    <a href="http://127.0.0.1:8000/student/export" class="btn btn-primary">Export</a>
                </div>
            </div>
            <hr>
        </div>
    </div>
        <div class="row">
        <div class="col-md-12">
            <table class="table table-striped"> 
                </thead class="thead-dark">
                    <th>Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email Id</th>
                    <th>City</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Action</th>
                </thead>
                <?php 
                    if(!empty($result)){
                        foreach ($result as $value) { ?>
                <tr>
                    <td><?php echo $value->id; ?></td>
                    <td><?php echo $value->first_name; ?></td>
                    <td><?php echo $value->last_name; ?></td>
                    <td><?php echo $value->email; ?></td>
                    <td><?php echo $value->city; ?></td>
                    <td><?php echo $value->created_at; ?></td>
                    <td><?php echo $value->updated_at; ?></td>
                    <td>
                        <a href="http://127.0.0.1:8000/student/edit/id/<?php echo $value->id; ?>">Edit</a>
                        <a onclick="javascript: return confirm('Are you sure you want to delete this Entry?');" href="http://127.0.0.1:8000/student/delete/id/<?php echo $value->id; ?>" style="color:red">Delete</a>
                    </td>
                </tr>
                <?php  
                        }
                    }
                ?>
            </table>
        </div>
    </div>
</div>
@endsection
