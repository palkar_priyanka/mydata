<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\student;
use Redirect;

class StudentController extends Controller
{	
	public function listing(){
    	$student = new student();
    	$data = $student->getStudent();
        $result = json_decode(json_encode($data, true));
		return view('student_listing')->with(array('result' => $result));
	}

    public function add(){
    	return view('student_add');
    }

    public function addAction(Request $request){
    	$studentData = array();
    	$request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required',
            'city'=>'required'
        ]);
    	$studentData['first_name'] = $request->first_name;
    	$studentData['last_name'] = $request->last_name;
    	$studentData['email'] = $request->email;
    	$studentData['city'] = $request->city;
    	$studentData['updated_at'] = date('Y-m-d h:i:s');
    	$studentData['created_at'] = date('Y-m-d h:i:s');
    	$student = new student();
    	$result = $student->addStudent($studentData);
        if($result){
           return Redirect::to('/student/listing')->with('message','Record deleted successfully'); 
        }else{
            return Redirect::to('/student/listing')->with('message','Something went wrong');
        }
    }

    public function edit(Request $request){
        $id = $request->segment(4);
        $student = new student();
        $data = $student->getStudentRecord($id);
        $result = json_decode(json_encode($data, true));
        return view('student_edit')->with(array('result' => $result));
    }

    public function editAction(Request $request){
        $studentData = array();
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required',
            'city'=>'required'
        ]);
        $studentData['id'] =$request->id;
        $studentData['first_name'] = $request->first_name;
        $studentData['last_name'] = $request->last_name;
        $studentData['email'] = $request->email;
        $studentData['city'] = $request->city;
        $studentData['updated_at'] = date('Y-m-d h:i:s');
        $studentData['created_at'] = date('Y-m-d h:i:s');
        $student = new student();
        $result = $student->updateStudentRecord($studentData);
        if($result){
           return Redirect::to('/student/listing')->with('message','Record deleted successfully'); 
        }else{
            return Redirect::to('/student/listing')->with('message','Something went wrong');
        }
    }

    public function delete(Request $request){
        $id = $request->segment(4);
        $student = new student();
        $result = $student->deleteStudent($id);
        if($result){
           return Redirect::to('/student/listing')->with('message','Record deleted successfully'); 
        }else{
            return Redirect::to('/student/listing')->with('message','Something went wrong');
        }
    }
}
