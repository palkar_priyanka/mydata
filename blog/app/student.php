<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Redirect;

class student extends Model
{
    // protected $fillable = ['first_name', 'last_name', 'email', 'city'];
    public function getStudent(){
        return $studentData = DB::table('students')->get();
    }

    public function addStudent($studentData){
        $student = new student;
        $student->first_name = $studentData['first_name'];
        $student->last_name = $studentData['last_name'];
        $student->email = $studentData['email'];
        $student->city = $studentData['city'];
        $student->updated_at = $studentData['updated_at'];
        $student->created_at = $studentData['created_at'];
        $student->save();
        return Redirect::to('/student/listing')->with('message','Record inserted successfully');
    }

    public function getStudentRecord($id){
        $student = new student;
        return $result = DB::table('students')->where('id',$id)->get();
    }

    public function updateStudentRecord($studentData){
        $student = new student;
        $id = $studentData['id'];
        $data = array(

        );
        $result = DB::table('students')->where('id',$id)->update(
            array(
                'first_name' => $studentData['first_name'],
                'last_name' => $studentData['last_name'],
                'email' => $studentData['email'],
                'city' => $studentData['city'],
                'updated_at' => $studentData['updated_at'],
                'created_at' => $studentData['created_at']
            ));
        return $result;
    }

    public function deleteStudent($id){
        $student = new student;
        return $result = student::where('id',$id)->delete();
    }
}
