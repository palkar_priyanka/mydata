<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/student/listing', 'StudentController@listing');
Route::get('/student/add', 'StudentController@add');
Route::post('/student/addAction', 'StudentController@addAction');
Route::get('/student/edit/id/{id}', 'StudentController@edit');
Route::post('/student/editAction', 'StudentController@editAction');
Route::get('/student/delete/id/{id}', 'StudentController@delete');
