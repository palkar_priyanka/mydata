<?php 

/*
1 2 3 4
8 7 6 5
9 10 11 12
16 15 14 13
*/

$val;

for($row = 1; $row <= 4; $row++){
	if($row%2 == 0){
		$val = $row*4;
		$diff = -1;
	}else{
		$val = ($row-1)*4 + 1;
		$diff = 1;
	}
	for($col = 1; $col <= 4; $col++){
		echo $val." ";
		$val = $val+$diff;
	}
	echo "<br>";
}

?>