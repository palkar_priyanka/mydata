Sr. No.

Candidate Name : Priyanka Vijay Palkar

Location : Airoli

Total Experience : 2 Years

Relevant Experience : 2 Years

Current Company : Zazpi tech pvt. ltd.

Current Designation : PHP Developer

Current CTC : 3.05 PA

Expected CTC : 3.50 PA

Notice Period : 7-10 days

Reasons To Quit : Profprofessional Growth

Communication Skills on the scale of 10 : 7

Domain Knowledge on the scale of 10 : 6

Previous Company 1 : Inscripts Pvt Ltd
	Designation : Associate Software Engineer.
	Experience : 2 Years

1. Why do you want to quit your current organisation?
-  Profprofessional Growth

2. Have you built websites from scratch without using wordpress?
-  Yes

3. Which framework have you worked on in PHP for instance - Laravel,
CodeIgniter, Symfony
- CodeIgniter , Basic knowledge of Laravel

4. Between Angular.js and React.js which one do you prefer and why?
- React.js . We can use this for frontend and mobile development too.

5. Do you want to build a web-based product and keep enhancing it with
new features like Facebook for instance OR do you want to work on creating
websites for different clients and then moving on?
- want to work on creating websites for different clients?

6. What was the toughest project/code you have handled so far?
- NO