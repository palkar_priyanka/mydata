<?php
	session_start();
$question = [
		["qid" => 1, "question" => "php is frontend programming language","options" => ["true","false"],"correct_answer" => 1],
		["qid" => 2, "question" => "php is server programming language","options" => ["true","false"],"correct_answer" => 0],
		["qid" => 3, "question" => "php is functional programming language","options" => ["true","false"],"correct_answer" => 1],
		["qid" => 4, "question" => "php supports following array functions","options" => ["map","reduce","filter","all of the above"],"correct_answer" => 1],
	];
// echo "<pre>";
// print_r($question);

shuffle($question);
$_SESSION['answer_sheet'] = [];

foreach ($question as $k => $v) {
	array_push(
		$_SESSION['answer_sheet'], ["qid" => $v['qid'],"correct_answer" => $v['correct_answer'],"submitted_answer" => null]
	);
}
?>

<form action="question_action.php">
	<?php foreach($question as $index => $que){ ?>
		<div>
			<?php echo $index + 1; ?> : <?php echo $que['question']; ?>
			<br>
			<?php
				$options = range(0, count($que['options']) - 1);
				shuffle($options);
				echo "<br>===========Options=========[Start]<br>";
				print_r($options);
				echo "<br>===========Options=========[End]<br>";
				foreach($options as $v){
			?>
			<input type="radio" name="answer[<?php echo $index; ?>]" value="<?php echo $v ?>"><?php echo $que['options'][$v]; ?>
			<br>
		<?php } ?>
		<hr>
		</div>
	<?php } ?>
	<input type="submit" value="submit">
</form>